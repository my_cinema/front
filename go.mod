module moviefront

go 1.14

require (
	github.com/go-vk-api/vk v0.0.0-20200129183856-014d9b8adc96
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.4.4 // indirect
	golang.org/x/oauth2 v0.0.0-20210113205817-d3ed898aa8a3
)
